
public class MyPoint {
private int x, y; 
	
	public MyPoint() 
	{
		x=y=0;
	}
	
	public MyPoint(int x,int y)	
	{
		this.x=x;
		this.y=y;
	}
	
	
	void setX(int k){ 
    	
    		this.x=x;
    }
	
	
	void setY(int z){
		this.y=y;
		
	}
	
	
    int getx(){
    	return x;
    	
    }
    
  
    int gety(){
    	return y;
    	
    }
    
    void setXY(int x,int y){
    	this.x=x;
    	this.y=y;
    }
    //A toString() method that returns a string description of the instance in the format �(x, y)�.
    public String toString()
	{
		return "("+x+", "+y+")";
	}
    //A method called distance(int x, int y) that returns the distance from this point to another point at the given (x, y) coordinates
    public double distance(MyPoint a)
	{
		return Math.sqrt((x-a.getx())*(x-a.getx())+(y-a.gety())*(y-a.gety()));
		
	}
}
