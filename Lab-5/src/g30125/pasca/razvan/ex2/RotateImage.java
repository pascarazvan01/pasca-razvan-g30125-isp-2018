package g30125.pasca.razvan.ex2;
public class RotateImage implements Image {
	
	private String fileName;
	public RotateImage(String fileName) {
		this.fileName=fileName;
	}
	
	public void display() {
		System.out.println("Display rotate "+fileName);
	}

}
