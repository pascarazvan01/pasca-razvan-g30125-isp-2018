package g30125.pasca.razvan.ex4;

public class TemperatureSensor extends Sensor {
	
	private static TemperatureSensor ts;
	
	private TemperatureSensor() {
		
	}
	
	public static TemperatureSensor getTemperatureSensor() {
		if(ts==null) {
			ts = new TemperatureSensor();
		}
		return ts;
	}
	
	int readValue() {
		int i=(int)(Math.random()*100);
		return i;
	}

}
