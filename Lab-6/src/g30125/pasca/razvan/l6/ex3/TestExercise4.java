package g30125.pasca.razvan.l6.ex3;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestExercise4 {
	 @Test
	 public void test() {
		Exercise4 sir = new Exercise4(new char[]{'r','y','m','p','2','5','V','Z'});
		assertEquals('5',sir.charAt(5));
		assertEquals(8,sir.length());
		assertEquals("rymp25VZ",sir.toString());
		Exercise4 sir2 = new Exercise4(new char[]{'W','O','N','e','t','a','p','1','2'});
		assertEquals("Ne",sir2.subSequence(3,4));
	 }
}