package g30125.pasca.razvan.l6.ex2;

import java.awt.Graphics;

public interface Shape {
	
    public abstract void draw(Graphics g);
    String getId();

	/*public void deleteById() {
	}/*/
}
